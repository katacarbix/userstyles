# userstyles
Custom stylesheets I made for websites that I use. You need a browser extension if you want to use these; I prefer [Stylus](https://github.com/openstyles/stylus#stylus).

## Mastodon
I like to use Mastodon's basic web UI (not the "advanced" one that looks like TweetDeck) on my computer and there were a few things about it that bugged me. You will need to change the domain name to match the instance that you use.
[![Install with Stylus](https://img.shields.io/badge/Install%20with-Stylus-00adad.svg)](https://raw.githubusercontent.com/reeseovine/userstyles/master/styles/masto-mods.user.css)
![before and after comparison animation](screenshots/mastodon.gif)

## New York Times Crossword free (syndication)
Some newspapers' websites embed a [crossword widget](https://nytsyn.pzzl.com/cwd) which is sourced from the NYT daily crossword. With this you can play the NYT crossword for free with the last week of puzzles available to select. The main issue is that it seems to be designed for use in `<iframe>`s and thus feels very cramped if you were to visit the page on its own. This userstyle is my attempt to fix that!  
[![Install with Stylus](https://img.shields.io/badge/Install%20with-Stylus-00adad.svg)](https://raw.githubusercontent.com/reeseovine/userstyles/master/styles/nyt-crossword.user.css)
<table>
  <tr>
    <td>
      <img src="screenshots/nytc-desktop.png" alt="crossword desktop preview">
    </td>
    <td>
      <img src="screenshots/nytc-mobile.png" alt="crossword mobile preview">
    </td>
  </tr>
</table>

## Searx Material
A theme for [Searx](https://searx.me) which follows Google's Material Design 3 guidelines and uses a color palette generated from Searx's logo. To install it, [visit the main repo](https://github.com/reeseovine/searx-material).
[![searx home - material dark](https://raw.githubusercontent.com/reeseovine/searx-material/main/preview/dark_home_simple.png)](https://github.com/reeseovine/searx-material)

## Picrew Image Maker Widescreen
⚠ Doesn't work currently!

Makes the picrew image maker fill the entire width of the page.  
[![Install with Stylus](https://img.shields.io/badge/Install%20with-Stylus-00adad.svg)](https://raw.githubusercontent.com/reeseovine/userstyles/master/styles/picrew.user.css)
![picrew preview 1](screenshots/picrew-1.png)
![picrew preview 2](screenshots/picrew-2.png)
The image maker shown above was made by [NUDEKAY](https://picrew.me/image_maker/395214).

## Twitter: Hide trends and follow suggestions
Hides the "What's Happening" (trending) and "Who to Follow" boxes on the right side of the Home timeline to minimize distractions.  
[![Install with Stylus](https://img.shields.io/badge/Install%20with-Stylus-00adad.svg)](https://raw.githubusercontent.com/reeseovine/userstyles/master/styles/twitter-trends.user.css)  
![twitter preview](screenshots/twitter-1.png)

## Netdata: Hide Cloud annoyances (deprecated)
**Heads up:** If you manage your own Netdata server you can turn off cloud by creating `/var/lib/netdata/cloud.d/cloud.conf` (or bind mount it if in docker) with the following contents:

```yaml
[global]
enabled = no
```

Since that is what I do now, I have no use for this style and I will no longer be maintaining it.

<details>
<summary>Show previous contents</summary>

[Netdata](https://www.netdata.cloud/) is a self-hosted server monitoring utility with an optional cloud service. If you don't plan on using it, this theme will remove all references to their cloud service and tidy up the UI.

By default it applies to URLs on the port `19999`, netdata's default port. You will need to change this if your URL is something different.  
[![Install with Stylus](https://img.shields.io/badge/Install%20with-Stylus-00adad.svg)](https://raw.githubusercontent.com/reeseovine/userstyles/master/styles/netdata-cloudless.user.css)  
![netdata preview](screenshots/netdata-1.png)

</details>

## license
`CC0` / public domain, so do whatever the hell you want!
